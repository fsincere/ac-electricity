# -*- coding: utf8 -*-
# python 3
# (C) Fabrice Sincère

from acelectricity import *

# rlc series circuit
Vin = Voltage(5)
Zr = Impedance(r=100)
Zl = Impedance(l=0.1)
Zc = Impedance(c=1e-6)
Zt = Zr+Zl+Zc  # impedances in series
I = Vin/Zt  # Ohm's law

# draw Bode plot
I.bode(magnitude_unit='default')
show()
