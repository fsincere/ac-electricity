# -*- coding: utf8 -*-
# python 3
# (C) Fabrice Sincère

from acelectricity import *

# rlc series circuit
Vin = Voltage(5)
Zr = Impedance(r=100)
Zl = Impedance(l=0.1)
Zc = Impedance(c=1e-6)
Zt = Zr+Zl+Zc  # impedances in series
I = Vin/Zt  # Ohm's law

# draw Bode plot
fig, ax1, ax2, mag, ph = I.bode(magnitude_unit='default')

fig.subplots_adjust(left=0.15, bottom=0.40, right=0.85, top=0.9)

ax_v_rms = plt.axes([0.15, 0.25, 0.6, 0.03])
ax_v_phase = plt.axes([0.15, 0.20, 0.6, 0.03])
ax_r = plt.axes([0.15, 0.15, 0.6, 0.03])
ax_l = plt.axes([0.15, 0.10, 0.6, 0.03])
ax_c = plt.axes([0.15, 0.05, 0.6, 0.03])

slider_v_rms = Slider(ax_v_rms, 'Vin (Vrms)', 0, 10, valinit=Vin.RMS,
                      valstep=0.1)
slider_v_phase = Slider(ax_v_phase, 'Vin (deg)', -180, 180, valinit=Vin.phase,
                        valstep=1)
slider_r = Slider(ax_r, 'r (Ω)', 10, 1000, valinit=Zr.r, valstep=1)
slider_l = Slider(ax_l, 'l (H)', 0.01, 1, valinit=Zl.l, valstep=0.01)
slider_c = Slider(ax_c, 'c (µF)', 0.1, 10, valinit=Zc.c*1e6, valstep=0.1)


def update(val):
    Vin.RMS = slider_v_rms.val
    Vin.phase = slider_v_phase.val
    Zr.r = slider_r.val
    Zl.l = slider_l.val
    Zc.c = slider_c.val*1e-6
    xvalues = mag.get_xdata()
    # I.rms() method, according to I.bode() parameters :
    # magnitude_unit='default'
    magnitudes = [I.rms(x) for x in xvalues]
    # I.phase_deg() method, according to I.bode() parameters :
    # phase_unit='degrees' (default)
    phases = [I.phase_deg(x) for x in xvalues]
    mag.set_ydata(magnitudes)
    ph.set_ydata(phases)

    # autoscale
    ax1.relim()
    ax1.autoscale_view()
    ax2.relim()
    ax2.autoscale_view()

slider_v_rms.on_changed(update)
slider_v_phase.on_changed(update)
slider_r.on_changed(update)
slider_l.on_changed(update)
slider_c.on_changed(update)

show()
