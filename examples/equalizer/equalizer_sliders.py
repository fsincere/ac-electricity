# equalizer bass/treble
# (C) Fabrice Sincère

import math
from acelectricity import *

__version__ = (0, 0, 2)
__author__ = "Fabrice Sincère <fabrice.sincere@ac-grenoble.fr>"


class amp():
    # keep reference
    pass


a = amp()

# bass amplifier (inverter) ##
gain_bass = 0  # dB          #
##############################

a.Abass = -10**(gain_bass/20)

# treble amplifier (inverter) ##
gain_treble = 0  # dB          #
################################

a.Atreble = -10**(gain_treble/20)

# second order low-pass, damping value 0.707
fc_low = 1000  # cutoff frequency -3 dB
fn_low = fc_low/1.000151  # natural frequency Hz
wn_low = 2*math.pi*fn_low  # natural frequency rad/s
H_low = Ratio(fw=lambda w: a.Abass/(1+2*1j*0.707*w/wn_low-(w/wn_low)**2))

# second order high-pass, damping value 0.707
fc_high = 1700  # cutoff frequency -3 dB
fn_high = fc_high*1.000151  # natural frequency Hz
wn_high = 2*math.pi*fn_high  # natural frequency rad/s
H_high = Ratio(fw=lambda w: -a.Atreble*(w/wn_high)**2
               / (1+2*1j*0.707*w/wn_high-(w/wn_high)**2))

# audio mixing
Output = H_low-H_high

# Bode plot
fig, ax1, ax2, mag, ph = Output.bode(title='Equalizer bass/treble')
fig.subplots_adjust(left=0.25, bottom=0.25, right=0.85, top=0.9)

ax1.set_ylim(ymin=-22, ymax=22)

ax_gain_bass = plt.axes([0.25, 0.10, 0.4, 0.03])
ax_gain_treble = plt.axes([0.25, 0.05, 0.4, 0.03])

slider_gain_bass = Slider(ax_gain_bass, 'bass Gain (dB)', -20, 20,
                          valinit=0, valstep=0.1)
slider_gain_treble = Slider(ax_gain_treble, 'treble Gain (dB)', -20, 20,
                            valinit=0, valstep=0.1)


def update(val):
    a.Abass = -10**(slider_gain_bass.val/20)
    a.Atreble = -10**(slider_gain_treble.val/20)

    xvalues = mag.get_xdata()
    # Output.db() method, according to Output.bode() parameters :
    # magnitude_unit='db' (default)
    # help(Output) for more information
    magnitudes = [Output.db(x) for x in xvalues]
    # Output.phase_deg() method, according to Output.bode() parameters :
    # phase_unit='degrees' (default)
    phases = [Output.phase_deg(x) for x in xvalues]
    mag.set_ydata(magnitudes)
    ph.set_ydata(phases)

    # autoscale
    # ax1.relim()
    # ax1.autoscale_view()
    ax2.relim()
    ax2.autoscale_view()


slider_gain_bass.on_changed(update)
slider_gain_treble.on_changed(update)

show()
