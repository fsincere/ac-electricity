# equalizer bass/treble
# (C) Fabrice Sincère

import math
from acelectricity import *

__version__ = (0, 0, 2)
__author__ = "Fabrice Sincère <fabrice.sincere@ac-grenoble.fr>"

# bass amplifier (inverter) ##
gain_bass = 10  # dB         #
##############################

Abass = -10**(gain_bass/20)

# treble amplifier (inverter) ##
gain_treble = 0  # dB          #
################################

Atreble = -10**(gain_treble/20)

# second order low-pass, damping value 0.707
fc_low = 1000  # cutoff frequency -3 dB
fn_low = fc_low/1.000151  # natural frequency Hz
wn_low = 2*math.pi*fn_low  # natural frequency rad/s
H_low = Ratio(fw=lambda w: Abass/(1+2*1j*0.707*w/wn_low-(w/wn_low)**2))
H_low.properties(fc_low)
H_low.bode(title='bass amp + low-pass filter')

# second order high-pass, damping value 0.707
fc_high = 1700  # cutoff frequency -3 dB
fn_high = fc_high*1.000151  # natural frequency Hz
wn_high = 2*math.pi*fn_high  # natural frequency rad/s
H_high = Ratio(fw=lambda w: -Atreble*(w/wn_high)**2
               / (1+2*1j*0.707*w/wn_high-(w/wn_high)**2))
H_high.properties(fc_high)
H_high.bode(title='treble amp + high-pass filter')

# audio mixing
Output = H_low-H_high
Output.bode(title='Equalizer')

show()
