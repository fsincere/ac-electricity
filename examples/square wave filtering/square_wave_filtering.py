# -*- coding: utf8 -*-
# python 3
# (C) Fabrice Sincère

from acelectricity import *

# global variable
# square wave frequency (Hz)
# fundamental frequency
f1 = 500


def square_wave_spectrum(w):

    # w : angular frequency (rad/s)
    def harmonic(n):
        # n : int
        if n == 0:
            # offset
            return 0.5
        elif n % 2 == 0:
            # even harmonic
            return 0
        else:
            # odd harmonic
            return 1/n

    global f1
    w1 = 2*math.pi*f1  # rad/s
    n = w/w1

    # round
    n_floor = int(n)
    # frequency component artificially enlarged
    width = 0.001  # 0.001 is suitable

    if n-n_floor < width:
        n = n_floor
        return harmonic(n)
    elif n-n_floor+1 < width:
        n = n_floor+1
        return harmonic(n)
    else:
        return 0

# Wave square voltage input
Vin = Voltage(fw=square_wave_spectrum)

# frequency scale
nmax = 20  # max harmonic
xmax = nmax*f1

# number of points (n0 : int >= 1)
n0 = 10   # 10 or 20 is suitable
n = nmax*2*n0 + 1

Vin.bode(xmin=0, xmax=xmax, n=n, xscale='linear', magnitude_unit='default',
         draw_phase=False, title='Wave square voltage input')

# band-pass filter
h = 7  # harmonic
# static gain, damping value, normal angular frequency
a, z, wn = 1, 0.05, h*(2*math.pi*f1)
H = Ratio(fw=lambda w: a*(2*z*1j*w/wn)/(1+2*z*1j*w/wn-(w/wn)**2))
Vout = H*Vin

H.bode(magnitude_label='Transfer function', title='Band-pass filter')
Vout.bode(xmin=0, xmax=xmax, n=n, xscale='linear', magnitude_unit='default',
          draw_phase=False, title='Vout')
Vout.bode(xmin=0, xmax=xmax, n=n, xscale='linear', yscale='log',
          magnitude_unit='default', draw_phase=False, title='Vout')
Vout.bode_real(xmin=0, xmax=xmax, n=n, xscale='linear')
Vout.bode_imag(xmin=0, xmax=xmax, n=n, xscale='linear')

show()
