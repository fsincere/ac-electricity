# -*- coding: utf8 -*-
# python 3
# (C) Fabrice Sincère

from acelectricity import *

# global variable
# square wave frequency (Hz)
# fundamental frequency
f1 = 500


def square_wave_spectrum(w):

    # w : angular frequency (rad/s)
    def harmonic(n):
        # n : int
        if n == 0:
            # offset
            return 0.5
        elif n % 2 == 0:
            # even harmonic
            return 0
        else:
            # odd harmonic
            return 1/n

    global f1
    w1 = 2*math.pi*f1  # rad/s
    n = w/w1

    # round
    n_floor = int(n)
    # frequency component artificially enlarged
    width = 0.001  # 0.001 is suitable

    if n-n_floor < width:
        n = n_floor
        return harmonic(n)
    elif n-n_floor+1 < width:
        n = n_floor+1
        return harmonic(n)
    else:
        return 0

# Wave square voltage input
Vin = Voltage(fw=square_wave_spectrum)

# frequency scale
nmax = 20  # max harmonic
xmax = nmax*f1

# number of points (n0 : int >= 1)
n0 = 10   # 10 or 20 is suitable
n = nmax*2*n0 + 1

Vin.bode(xmin=0, xmax=xmax, n=n, xscale='linear', magnitude_unit='default',
         draw_phase=False, title='Wave square voltage input')

# band-pass filter
h = 7  # harmonic
# static gain, damping value, normal angular frequency
a, z, wn = 1, 0.05, h*(2*math.pi*f1)
H = Ratio(fw=lambda w: a*(2*z*1j*w/wn)/(1+2*z*1j*w/wn-(w/wn)**2))
Vout = H*Vin

fig, ax1, ax2, mag, ph = H.bode(xmin=0, title='Band-pass filter')
fig.subplots_adjust(left=0.15, bottom=0.30, right=0.85, top=0.9)

ax_a = plt.axes([0.20, 0.15, 0.6, 0.03])
ax_z = plt.axes([0.20, 0.10, 0.6, 0.03])
ax_fn = plt.axes([0.20, 0.05, 0.6, 0.03])

slider_a = Slider(ax_a, 'Static gain', 0.01, 100, valinit=a, valstep=0.01)
slider_z = Slider(ax_z, 'Damping value', 0.01, 2, valinit=z, valstep=0.01)
slider_fn = Slider(ax_fn, 'fn (Hz)', f1, 20*f1, valinit=h*f1, valstep=f1/10)


def update(val):
        global a, z, wn
        a = slider_a.val

        if sliderb_a.val != a:
            sliderb_a.set_val(a)
        z = slider_z.val
        if sliderb_z.val != z:
            sliderb_z.set_val(z)
        fn = slider_fn.val
        wn = fn*2*math.pi
        if sliderb_fn.val != fn:
            sliderb_fn.set_val(fn)
        xvalues = mag.get_xdata()
        magnitudes = [H.db(x) for x in xvalues]
        phases = [H.phase_deg(x) for x in xvalues]
        mag.set_ydata(magnitudes)
        ph.set_ydata(phases)

        # autoscale
        ax1.relim()
        ax1.autoscale_view()
        ax2.relim()
        ax2.autoscale_view()


slider_a.on_changed(update)
slider_z.on_changed(update)
slider_fn.on_changed(update)

figb, ax1b, ax2b, magb, phb = Vout.bode(xmin=0, xmax=xmax, n=n, xscale='linear', magnitude_unit='default', draw_phase=False, title='Vout')

figb.subplots_adjust(left=0.15, bottom=0.30, right=0.85, top=0.9)

axb_a = plt.axes([0.20, 0.15, 0.6, 0.03])
axb_z = plt.axes([0.20, 0.10, 0.6, 0.03])
axb_fn = plt.axes([0.20, 0.05, 0.6, 0.03])

sliderb_a = Slider(axb_a, 'Static gain', 0.01, 100, valinit=a, valstep=0.01)
sliderb_z = Slider(axb_z, 'Damping value', 0.01, 2, valinit=z, valstep=0.01)
sliderb_fn = Slider(axb_fn, 'fn (Hz)', f1, 20*f1, valinit=h*f1, valstep=f1/10)


def updateb(val):
        global a, z, wn
        a = sliderb_a.val
        if slider_a.val != a:
            slider_a.set_val(a)

        z = sliderb_z.val
        if slider_z.val != z:
            slider_z.set_val(z)

        fn = sliderb_fn.val
        wn = fn*2*math.pi
        if slider_fn.val != fn:
            slider_fn.set_val(fn)

        xvalues = magb.get_xdata()
        magnitudes = [Vout.rms(x) for x in xvalues]
        phases = [H.phase_deg(x) for x in xvalues]
        magb.set_ydata(magnitudes)
        phb.set_ydata(phases)

        # autoscale
        ax1b.relim()
        ax1b.autoscale_view()
        ax2b.relim()
        ax2b.autoscale_view()

sliderb_a.on_changed(updateb)
sliderb_z.on_changed(updateb)
sliderb_fn.on_changed(updateb)

show()
