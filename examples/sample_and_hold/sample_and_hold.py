# python3
# (C) Fabrice Sincère

from acelectricity import *

fs = 1000.0  # sampling frequency
Ts = 1/fs

# sample-and-hold
B0 = Ratio(fw=lambda w: (1-cmath.exp(-1j*w*Ts))/(Ts*1j*w))
B0.bode(xmin=1e-3*fs, xmax=5*fs, xscale='linear', magnitude_unit='default',
        title='Sample-and-hold order 0')

# first order analog low-pass
tau = 10*Ts
Hlp = Ratio(fw=lambda w: 1/(1+1j*w*tau))
Hlp.bode(xmin=1e-3*fs, xmax=5*fs,
         title='First order analog low-pass')

# Sample-and-hold and first order low-pass
G = B0*Hlp
G.bode(xmin=1e-3*fs, xmax=5*fs,
       title='Sample-and-hold and first order analog low-pass')
G.bode(xmin=1e-3*fs, xmax=0.5*fs, xscale='linear', magnitude_unit='default',
       title='Sample-and-hold and first order analog low-pass')

# Z transfer function
H = Ratio.digital_filter(fs=fs, b=[0, 1-math.exp(-Ts/tau)],
                         a=[1, -math.exp(-Ts/tau)])
H.bode(xmin=1e-3*fs, xmax=0.5*fs, xscale='linear', magnitude_unit='default',
       title='Z transfer function')
H.bode(xmin=1e-3*fs, xmax=0.5*fs, xscale='linear', magnitude_unit='db',
       title='Z transfer function')
show()
