# -*- coding: utf-8 -*-
# python 3
# (C) Fabrice Sincère

from acelectricity import *

# first order low-pass filter
wn1 = 10000
Hlp = Ratio(fw=lambda w: 1/(1+1j*w/wn1))
# first order high-pass filter
wn2 = 10000
Hhp = Ratio(fw=lambda w: 1/(1+1j*wn2/w))
Hs = Hlp*Hhp  # cascaded series filters
Hs.bode(title='Cascaded series filters')
Hp = Hlp+Hhp  # parallel filters
Hp.bode(title='Parallel filters')
show()
