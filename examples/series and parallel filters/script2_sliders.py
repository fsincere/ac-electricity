# -*- coding: utf-8 -*-
# python 3
# (C) Fabrice Sincère

from acelectricity import *

# first order low-pass filter
wn1 = 10000
Hlp = Ratio(fw=lambda w: 1/(1+1j*w/wn1))
# first order high-pass filter
wn2 = 10000
Hhp = Ratio(fw=lambda w: 1/(1+1j*wn2/w))
Hs = Hlp*Hhp  # cascaded series filters

fig, ax1, ax2, mag, ph = Hs.bode(magnitude_label='Transfer function',
                                 title='Cascaded series filters')

fig.subplots_adjust(left=0.15, bottom=0.30, right=0.85, top=0.9)

ax_fn1 = plt.axes([0.25, 0.10, 0.4, 0.03])
ax_fn2 = plt.axes([0.25, 0.05, 0.4, 0.03])

slider_fn1 = Slider(ax_fn1, 'fn low-pass filter (Hz)', 100, 1e4, valinit=1e3)
slider_fn2 = Slider(ax_fn2, 'fn high-pass filter (Hz)', 100, 1e4, valinit=1e3)


def update(val):
    global wn1, wn2
    fn1 = slider_fn1.val
    wn1 = fn1*2*math.pi
    fn2 = slider_fn2.val
    wn2 = fn2*2*math.pi

    xvalues = mag.get_xdata()
    # Hs.db() method, according to Hs.bode() parameters :
    # magnitude_unit='db' (default)
    # help(Hs) for more information
    magnitudes = [Hs.db(x) for x in xvalues]
    # Hs.phase_deg() method, according to Hs.bode() parameters :
    # phase_unit='degrees' (default)
    phases = [Hs.phase_deg(x) for x in xvalues]
    mag.set_ydata(magnitudes)
    ph.set_ydata(phases)

slider_fn1.on_changed(update)
slider_fn2.on_changed(update)

ax_button = plt.axes([0.8, 0.05, 0.15, 0.06])
button = Button(ax_button, 'Autoscale')


def autoscale(event):
    ax1.relim()
    ax1.autoscale_view()
    ax2.relim()
    ax2.autoscale_view()

button.on_clicked(autoscale)
show()
