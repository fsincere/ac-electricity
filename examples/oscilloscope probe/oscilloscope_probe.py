# -*- coding: utf-8 -*-
# python 3
# (C) Fabrice Sincère

from acelectricity import *

# adjustable passive oscilloscope probe 10:1

Vin = Voltage()
# probe
Zr = Impedance(r=9e6)
Zc = Impedance(c=5e-12)  # adjustable
# oscilloscope input
Zre = Impedance(r=1e6)
Zce = Impedance(c=90e-12)

Zosc = 1/(1/Zce + 1/Zre)  # impedances in parallel
# or Zosc = Zce//Zre
print(Zosc)

Zprobe = Zr//Zc  # impedances in parallel
print(Zprobe)

Zeq = Zprobe+Zosc  # impedances in series
print(Zeq(2000))  # @ 2000 Hz

Vosc = (Zosc/Zeq)*Vin  # voltage divider

H = Vosc/Vin  # transfer function

# draw Bode plot
H.bode(magnitude_label='Transfer function',
       title='Adjustable passive oscilloscope probe 10:1')

# draw Bode plot with slider
fig, ax1, ax2, mag, ph = H.bode(magnitude_label='Transfer function', title='Adjustable passive oscilloscope probe 10:1')
fig.subplots_adjust(left=0.15, bottom=0.20, right=0.85, top=0.9)

ax_c = plt.axes([0.15, 0.05, 0.4, 0.03])
slider_c = Slider(ax_c, 'c (pF)', 1, 20, valinit=Zc.c*1e12, valstep=0.1)


def update(val):
    Zc.c = slider_c.val*1e-12
    xvalues = mag.get_xdata()
    # H.db() method, according to H.bode() parameters :
    # magnitude_unit='db' (default)
    # help(H) for more information
    magnitudes = [H.db(x) for x in xvalues]
    # H.phase_deg() method, according to H.bode() parameters :
    # phase_unit='degrees' (default)
    phases = [H.phase_deg(x) for x in xvalues]
    mag.set_ydata(magnitudes)
    ph.set_ydata(phases)

    # autoscale
    ax1.relim()
    ax1.autoscale_view()
    ax2.relim()
    ax2.autoscale_view()

slider_c.on_changed(update)
show()
