# -*- coding: utf-8 -*-
# python 3
# (C) Fabrice Sincère

from acelectricity import *

wn_lp = 10000  # rad/s
Hlp = Ratio(fw=lambda w: 1 if w < wn_lp else 0.001)

wn_hp = 1000
Hhp = Ratio(fw=lambda w: 10 if w > wn_hp else 0.001)

wn1_bp, wn2_bp = 1000, 10000
Hbp = Ratio(fw=lambda w: 1 if wn2_bp > w > wn1_bp else 0.001)

wn1, wn2 = 4000, 5000
Hnotch = Ratio(fw=lambda w: 0.001 if wn2 > w > wn1 else 1)

wn = 10000
Hallpass = Ratio(fw=lambda w: 1j if w > wn else -1j)

# cascaded series filters
Hs = Hlp*Hhp
Hs.bode(title='Ideal cascaded series filters')

# add sliders
fig, ax1, ax2, mag, ph = Hs.bode(magnitude_label='Transfer function',
                                 title='Ideal cascaded series filters')

fig.subplots_adjust(left=0.15, bottom=0.30, right=0.85, top=0.9)

ax_lp = plt.axes([0.25, 0.10, 0.4, 0.03])
ax_hp = plt.axes([0.25, 0.05, 0.4, 0.03])

slider_lp = Slider(ax_lp, 'fn low-pass filter (Hz)', 100, 1e4, valinit=8000)
slider_hp = Slider(ax_hp, 'fn high-pass filter (Hz)', 100, 1e4, valinit=1000)


def update(val):
    global wn_lp, wn_hp
    fn_lp = slider_lp.val
    wn_lp = fn_lp*2*math.pi
    fn_hp = slider_hp.val
    wn_hp = fn_hp*2*math.pi

    xvalues = mag.get_xdata()
    # Hs.db() method, according to Hs.bode() parameters :
    # magnitude_unit='db' (default)
    # help(Hs) for more information
    magnitudes = [Hs.db(x) for x in xvalues]
    # Hs.phase_deg() method, according to Hs.bode() parameters :
    # phase_unit='degrees' (default)
    phases = [Hs.phase_deg(x) for x in xvalues]
    mag.set_ydata(magnitudes)
    ph.set_ydata(phases)

    # autoscale
    ax1.relim()
    ax1.autoscale_view()
    ax2.relim()
    ax2.autoscale_view()

slider_lp.on_changed(update)
slider_hp.on_changed(update)

show()
