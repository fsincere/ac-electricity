# -*- coding: utf-8 -*-
# python 3
# (C) Fabrice Sincère

from acelectricity import *


def lp(w):
    wn = 10000
    return 1 if w < wn else 0.001


def hp(w):
    wn = 1000
    return 10 if w > wn else 0.001


def bp(w):
    wn1, wn2 = 1000, 10000
    return 1 if wn2 > w > wn1 else 0.001


def notch(w):
    wn1, wn2 = 4000, 5000
    return 0.001 if wn2 > w > wn1 else 1


def allpass(w):
    wn = 10000
    return 1j if w > wn else -1j

Hlp = Ratio(fw=lp)
Hhp = Ratio(fw=hp)
Hbp = Ratio(fw=bp)
Hnotch = Ratio(fw=notch)
Hallpass = Ratio(fw=allpass)

Hlp.bode(title='Ideal low-pass filter')
Hhp.bode(title='Ideal high-pass filter')
Hbp.bode(title='Ideal band-pass filter')
Hnotch.bode(title='Ideal notch filter')
Hallpass.bode(title='Ideal all-pass filter')

# parallel filters
(Hhp+Hlp).bode(title='Ideal parallel filters')
# cascaded series filters
(Hhp*Hlp*Hallpass).bode(title='Ideal cascaded series filters')
show()
