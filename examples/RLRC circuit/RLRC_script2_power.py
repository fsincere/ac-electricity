# -*- coding: utf-8 -*-
# python 3
# (C) Fabrice Sincère

from acelectricity import *

print('''
                  VL
           <---------------------
                          L1
            +------+    _  _  _
      --->--| R1   |---/ \/ \/ \-----+-------+
        IL  +------+                 |       |
                                     |       |
    ^                             IR v       v IC
    |                                |       |      ^
    |                              +---+     |  C1  |
    |                              |   |   -----    |
Vin |                              |R2 |   -----    | Vout
    |                              |   |     |      |
    |                              +---+     |      |
    |                                |       |
                                     |       |
      -------------------------------+-------+

''')

Vin = Voltage(5)  # Vrms
Zr1 = Impedance(r=180)
Zl1 = Impedance(l=0.1)
Zr2 = Impedance(r=2200)
Zc1 = Impedance(c=330e-9)

Zeq1 = 1/(1/Zr2 + 1/Zc1)  # impedances in parallel
# or Zeq1 = Zr2//Zc1
Zeq = Zr1 + Zl1 + Zeq1  # impedances in series

IL = Vin/Zeq  # Ohm's law
Vout = IL*Zeq1  # Ohm's law
VL = Vin-Vout  # Kirchhoff’s voltage law
IC = Vout/Zc1  # Ohm's law
IR = IL-IC  # Kirchhoff’s current law
H = Vout/Vin  # transfer function
S = IL*Vin  # input source complex power
S.properties(1000)

law = Law()
Sr1 = law.Joule(z=Zr1, i=IL)
print(Sr1)
print(Zr1*IL*IL)
Sr2 = law.Joule(z=Zr2, v=Vout)
print(Sr2)
Sl1 = Zl1*IL*IL
print(Sl1)
Sc1 = Zc1*IC*IC
print(Sc1)
print(Sr1+Sr2+Sc1+Sl1)

# draw Bode plot and save datas
S.bode(magnitude_unit='default', title='Active power Bode plot',
       filename='w.csv')
S.bode_va(filename='va.csv')
S.bode_var(filename='var.csv')
S.bode_pf(filename='pf.csv')
show()
