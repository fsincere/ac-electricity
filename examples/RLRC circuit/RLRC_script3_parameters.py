# -*- coding: utf-8 -*-
# python 3
# (C) Fabrice Sincère

from acelectricity import *

print('''
                  VL
           <---------------------
                          L1
            +------+    _  _  _
      --->--| R1   |---/ \/ \/ \-----+-------+
        IL  +------+                 |       |
                                     |       |
    ^                             IR v       v IC
    |                                |       |      ^
    |                              +---+     |  C1  |
    |                              |   |   -----    |
Vin |                              |R2 |   -----    | Vout
    |                              |   |     |      |
    |                              +---+     |      |
    |                                |       |
                                     |       |
      -------------------------------+-------+

''')

Vin = Voltage(5)  # Vrms
Zr1 = Impedance(r=180)
Zl1 = Impedance(l=0.1)
Zr2 = Impedance(r=2200)
Zc1 = Impedance(c=330e-9)

Zeq1 = 1/(1/Zr2 + 1/Zc1)  # impedances in parallel
# or Zeq1 = Zr2//Zc1
print(Zeq1)

Zeq = Zr1 + Zl1 + Zeq1  # impedances in series
print(Zeq(2000))  # @ 2000 Hz

IL = Vin/Zeq  # Ohm's law
IL.properties(2000)

Vout = IL*Zeq1  # Ohm's law
VL = Vin-Vout  # Kirchhoff’s voltage law
IC = Vout/Zc1  # Ohm's law
IR = IL-IC  # Kirchhoff’s current law
H = Vout/Vin  # transfer function

# draw Bode plot and save datas
H.bode(title='Vout/Vin transfer function', filename='h.csv')
IL.bode(magnitude_unit='default', yscale='linear', title='IL current')

Vin.RMS = 10
Vin.phase = 45
Zr1.r = 100
Zl1.l = 0.22
Zr2.r = 1000
Zc1.c = 100e-9
H.bode(title='Vout/Vin transfer function', filename='h2.csv')
IL.bode(magnitude_unit='default', yscale='linear', title='IL current')

for Zr2.r in [10, 100, 1e3, 1e4]:
    H.bode(title="Vout/Vin with R2={} Ω".format(Zr2.r))
show()
