# -*- coding: utf-8 -*-
# python 3
# (C) Fabrice Sincère

from acelectricity import *

print('''
                  VL
           <---------------------
                          L1
            +------+    _  _  _
      --->--| R1   |---/ \/ \/ \-----+-------+
        IL  +------+                 |       |
                                     |       |
    ^                             IR v       v IC
    |                                |       |      ^
    |                              +---+     |  C1  |
    |                              |   |   -----    |
Vin |                              |R2 |   -----    | Vout
    |                              |   |     |      |
    |                              +---+     |      |
    |                                |       |
                                     |       |
      -------------------------------+-------+

''')

Vin = Voltage(5)  # Vrms
Zr1 = Impedance(r=180)
Zl1 = Impedance(l=0.1)
Zr2 = Impedance(r=2200)
Zc1 = Impedance(c=330e-9)

Zeq1 = 1/(1/Zr2 + 1/Zc1)  # impedances in parallel
# or Zeq1 = Zr2//Zc1
print(Zeq1)

Zeq = Zr1 + Zl1 + Zeq1  # impedances in series
print(Zeq(2000))  # @ 2000 Hz

IL = Vin/Zeq  # Ohm's law
IL.properties(2000)

Vout = IL*Zeq1  # Ohm's law
VL = Vin-Vout  # Kirchhoff’s voltage law
IC = Vout/Zc1  # Ohm's law
IR = IL-IC  # Kirchhoff’s current law
H = Vout/Vin  # transfer function

# draw Bode plot
fig, ax1, ax2, mag, ph = H.bode(title='Vout/Vin transfer function')
fig.subplots_adjust(left=0.15, bottom=0.35, right=0.85, top=0.9)

ax_r1 = plt.axes([0.15, 0.20, 0.4, 0.03])
ax_l1 = plt.axes([0.15, 0.15, 0.4, 0.03])
ax_r2 = plt.axes([0.15, 0.10, 0.4, 0.03])
ax_c1 = plt.axes([0.15, 0.05, 0.4, 0.03])

slider_r1 = Slider(ax_r1, 'r1 (Ω)', 10, 300, valinit=Zr1.r, valstep=1)
slider_l1 = Slider(ax_l1, 'l1 (H)', 0.01, 1, valinit=Zl1.l, valstep=0.01)
slider_r2 = Slider(ax_r2, 'r2 (Ω)', 1e3, 1e4, valinit=Zr2.r, valstep=10)
slider_c1 = Slider(ax_c1, 'c1 (nF)', 100, 1000, valinit=Zc1.c*1e9, valstep=1)


def update(val):
    Zr1.r = slider_r1.val
    Zl1.l = slider_l1.val
    Zr2.r = slider_r2.val
    Zc1.c = slider_c1.val*1e-9
    xvalues = mag.get_xdata()
    # H.db() method, according to H.bode() parameters :
    # magnitude_unit='db' (default)
    # help(H) for more information
    magnitudes = [H.db(x) for x in xvalues]
    # H.phase_deg() method, according to H.bode() parameters :
    # phase_unit='degrees' (default)
    phases = [H.phase_deg(x) for x in xvalues]
    mag.set_ydata(magnitudes)
    ph.set_ydata(phases)

slider_r1.on_changed(update)
slider_l1.on_changed(update)
slider_r2.on_changed(update)
slider_c1.on_changed(update)

ax_button = plt.axes([0.7, 0.10, 0.15, 0.06])
button = Button(ax_button, 'Autoscale')


def autoscale(event):
    ax1.relim()
    ax1.autoscale_view()
    ax2.relim()
    ax2.autoscale_view()

button.on_clicked(autoscale)
show()
