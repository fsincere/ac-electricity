# -*- coding: utf-8 -*-
# python 3
# (C) Fabrice Sincère

from acelectricity import *

# y(n) = (x(n)+x(n-1)+...+x(n-7))/8

fs = 10000  # sampling rate (Hz)
H = Ratio.digital_filter(fs=fs, b=[1/8]*8)

# draw Bode plot and save datas
H.bode(xmin=0, xmax=fs/2, xscale='linear', magnitude_unit='default',
       title='FIR digital filter', filename='fir.csv')
H.bode(xmin=0, xmax=fs/2, xscale='linear', magnitude_unit='db',
       title='FIR digital filter', filename='fir2.csv')
show()
