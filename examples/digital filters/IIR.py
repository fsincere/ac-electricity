# -*- coding: utf-8 -*-
# python 3
# (C) Fabrice Sincère

from acelectricity import *

# y(n) = 0.1x(n) +1.6y(n-1) -0.7y(n-2)

fs = 100000  # sampling rate (Hz)
H = Ratio.digital_filter(fs=fs, b=[0.1], a=[1, -1.6, 0.7])

# draw Bode plot and save datas
H.bode(xmin=0, xmax=fs/2, xscale='linear', title='IIR digital filter',
       filename='iir.csv')
show()
