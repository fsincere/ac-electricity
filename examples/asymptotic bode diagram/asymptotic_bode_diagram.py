# -*- coding: utf-8 -*-
# python 3
# (C) Fabrice Sincère

from acelectricity import *


def lp1(w):
    # first order low-pass filter 1/(1+1j*w/wn)
    # asymptotic approximation
    wn = 1000
    return 1 if w < wn else 1/(1j*w/wn)


def lp2(w):
    wn = 30000
    return 1 if w < wn else 1/(1j*w/wn)


Hlp1 = Ratio(fw=lp1)
Hlp2 = Ratio(fw=lp2)

# cascaded series filters
(Hlp1*Hlp2).bode(title='Asymptotic Bode plot')
show()
