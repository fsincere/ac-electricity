# -*- coding: utf-8 -*-
# python 3
# (C) Fabrice Sincère

from acelectricity import *

# first order low-pass filter 1/(1+1j*w/wn)
# asymptotic approximation
wn1 = 1000  # rad/s
Hlp1 = Ratio(fw=lambda w: 1 if w < wn1 else 1/(1j*w/wn1))

wn2 = 30000
Hlp2 = Ratio(fw=lambda w: 1 if w < wn2 else 1/(1j*w/wn2))

# cascaded series filters
Hs = Hlp1*Hlp2
Hs.bode(title='Asymptotic Bode plot')

# add sliders
fig, ax1, ax2, mag, ph = Hs.bode(xmax=1e6, magnitude_label='Transfer function',
                                 title='Cascaded series filters')

fig.subplots_adjust(left=0.15, bottom=0.30, right=0.85, top=0.9)

ax_fn1 = plt.axes([0.25, 0.10, 0.4, 0.03])
ax_fn2 = plt.axes([0.25, 0.05, 0.4, 0.03])

slider_fn1 = Slider(ax_fn1, 'fn1 low-pass filter (Hz)', 100, 1e4, valinit=1e3)
slider_fn2 = Slider(ax_fn2, 'fn2 low-pass filter (Hz)', 1e4, 1e5, valinit=3e4)


def update(val):
    global wn1, wn2
    fn1 = slider_fn1.val
    wn1 = fn1*2*math.pi
    fn2 = slider_fn2.val
    wn2 = fn2*2*math.pi

    xvalues = mag.get_xdata()
    # Hs.db() method, according to Hs.bode() parameters :
    # magnitude_unit='db' (default)
    # help(Hs) for more information
    magnitudes = [Hs.db(x) for x in xvalues]
    # Hs.phase_deg() method, according to Hs.bode() parameters :
    # phase_unit='degrees' (default)
    phases = [Hs.phase_deg(x) for x in xvalues]
    mag.set_ydata(magnitudes)
    ph.set_ydata(phases)

    # autoscale
    ax1.relim()
    ax1.autoscale_view()
    ax2.relim()
    ax2.autoscale_view()

slider_fn1.on_changed(update)
slider_fn2.on_changed(update)

show()
