# -*- coding: utf-8 -*-
# python 3
# (C) Fabrice Sincère

from acelectricity import *

# static gain, damping value, normal angular frequency
a, z, wn = 10, 0.1, 1000*2*math.pi
H = Ratio(fw=lambda w: a*(2*z*1j*w/wn)/(1+2*z*1j*w/wn-(w/wn)**2))

fig, ax1, ax2, mag, ph = H.bode(magnitude_label='Transfer function',
                                title='Second order band-pass filter')

fig.subplots_adjust(left=0.15, bottom=0.30, right=0.85, top=0.9)

ax_a = plt.axes([0.25, 0.15, 0.4, 0.03])
ax_z = plt.axes([0.25, 0.10, 0.4, 0.03])
ax_fn = plt.axes([0.25, 0.05, 0.4, 0.03])

slider_a = Slider(ax_a, 'Static gain (/)', 0.01, 100, valinit=a, valstep=0.01)
slider_z = Slider(ax_z, 'Damping value (/)', 0.01, 2, valinit=z, valstep=0.01)
slider_fn = Slider(ax_fn, 'Normal frequency (Hz)', 100, 10000,
                   valinit=1000, valstep=100)


def update(val):
    global a, z, wn
    a = slider_a.val
    z = slider_z.val
    fn = slider_fn.val
    wn = fn*2*math.pi
    xvalues = mag.get_xdata()
    # H.db() method, according to H.bode() parameters :
    # magnitude_unit='db' (default)
    # help(H) for more information
    magnitudes = [H.db(x) for x in xvalues]
    # H.phase_deg() method, according to H.bode() parameters :
    # phase_unit='degrees' (default)
    phases = [H.phase_deg(x) for x in xvalues]
    mag.set_ydata(magnitudes)
    ph.set_ydata(phases)

slider_a.on_changed(update)
slider_z.on_changed(update)
slider_fn.on_changed(update)

ax_button = plt.axes([0.8, 0.10, 0.15, 0.06])
button = Button(ax_button, 'Autoscale')


def autoscale(event):
    ax1.relim()
    ax1.autoscale_view()
    ax2.relim()
    ax2.autoscale_view()

button.on_clicked(autoscale)
show()
