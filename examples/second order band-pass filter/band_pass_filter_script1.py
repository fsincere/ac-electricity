# -*- coding: utf-8 -*-
# python 3
# (C) Fabrice Sincère

from acelectricity import *

# static gain, damping value, normal angular frequency
a, z, wn = 10, 0.1, 1000*2*math.pi
H = Ratio(fw=lambda w: a*(2*z*1j*w/wn)/(1+2*z*1j*w/wn-(w/wn)**2))
H.bode(title='Second order band-pass filter', filename='H.csv')
a, z, wn = 100, 0.5, 10000
H.bode(title='Second order band-pass filter', filename='H2.csv')
show()
