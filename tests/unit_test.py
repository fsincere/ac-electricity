# -*- coding: utf8 -*-
# python 3
# (C) Fabrice Sincère

import math
import cmath
import random
import unittest
import acelectricity as ac

print(ac.__version__)

# label
f = "complex"  # float and complex
y = "Admittance"
i = "Current"
p = "Power"
z = "Impedance"
r = "Ratio"
v = "Voltage"

typeerr = "TypeError"
diverr = 'ZeroDivisionError'

FREQ = random.uniform(1, 1000)
dict1_obj = dict()

dict1_obj[f] = random.uniform(-1000, 1000)+1j*random.uniform(-1000, 1000)
dict1_obj[y] = ac.Admittance(r=1000)+ac.Admittance(l=0.1)
+ac.Admittance(c=100e-9)
dict1_obj[i] = ac.Current(2, 30)
dict1_obj[p] = ac.Power(1000+200j)
dict1_obj[z] = ac.Impedance(r=1500)+ac.Impedance(l=0.22)+ac.Impedance(c=470e-9)
dict1_obj[r] = ac.Ratio(1+2j)
dict1_obj[v] = ac.Voltage(5, 45)

print(dict1_obj)

dict2_obj = dict()
dict2_obj[f] = random.uniform(-1000, 1000)+1j*random.uniform(-1000, 1000)
dict2_obj[y] = ac.Admittance(r=3000)+ac.Admittance(l=0.2)
+ac.Admittance(c=150e-9)
dict2_obj[i] = ac.Current(3, -15)
dict2_obj[p] = ac.Power(2000-100j)
dict2_obj[z] = ac.Impedance(r=3300)+ac.Impedance(l=0.32)+ac.Impedance(c=680e-9)
dict2_obj[r] = ac.Ratio(1.5-5j)
dict2_obj[v] = ac.Voltage(10, -30)


class Test(unittest.TestCase):

    def test_PrefixNotation(self):
        # test function PrefixNotation
        self.assertAlmostEqual(ac.PrefixNotation(20, 'm'), 0.02)
        self.assertAlmostEqual(ac.PrefixNotation(50, 'k'), 50000)
        self.assertAlmostEqual(ac.PrefixNotation(-10000), -10000)

    def test_getfrequency(self):
        self.assertAlmostEqual(ac.getfrequency(1000, 'Hz'),
                               (1000, 6283.185307179586))
        self.assertAlmostEqual(ac.getfrequency(1, 'kHz'),
                               (1000, 6283.185307179586))
        self.assertAlmostEqual(ac.getfrequency(1000, 'rad/s'),
                               (159.15494309189535, 1000))

    def test_class_Impedance(self):
        # test class Impedance
        r1 = ac.Impedance(r=22000)
        r1
        print(r1)
        self.assertIsInstance(r1, ac.Impedance)
        self.assertAlmostEqual(r1(1000), 22000)

        r2 = ac.Impedance(r=4700)
        self.assertIsInstance(r2, ac.Impedance)
        self.assertAlmostEqual(r2(4700), 4700)

        with self.assertRaises(ValueError):
            ac.Impedance(r=-22000)
        with self.assertRaises(ValueError):
            ac.Impedance(l=-22000)
        with self.assertRaises(ValueError):
            ac.Impedance(c=-22000)

        r3 = r1+r2
        self.assertIsInstance(r3, ac.Impedance)
        self.assertAlmostEqual(r3(1000), 26700)

        r3 = r1-r2
        self.assertIsInstance(r3, ac.Impedance)
        self.assertAlmostEqual(r3(1000), 17300)

        r3 = r2-r1
        self.assertIsInstance(r3, ac.Impedance)
        self.assertAlmostEqual(r3(1000), -17300)

        r3 = -r1+r2
        self.assertIsInstance(r3, ac.Impedance)
        self.assertAlmostEqual(r3(1000), -17300)

        r3 = +r1
        self.assertIsInstance(r3, ac.Impedance)
        self.assertIsNot(r3, r1)
        self.assertAlmostEqual(r3(1000), 22000)

        r3 = -r1
        self.assertIsInstance(r3, ac.Impedance)
        self.assertAlmostEqual(r3(1000), -22000)

        r3 = +r1-r2
        self.assertIsInstance(r3, ac.Impedance)
        self.assertAlmostEqual(r3(1000), 17300)

        r3 = r1//r2
        self.assertIsInstance(r3, ac.Impedance)
        self.assertAlmostEqual(r3(1000), 3872.659176)

        ra = ac.Impedance(r=1000)
        rb = ac.Impedance(r=100)-ac.Impedance(r=100)  # 0 ohm
        rc = ra//rb
        self.assertIsInstance(rc, ac.Impedance)
        with self.assertRaises(ZeroDivisionError):
            rc(1000)

        i1 = ac.Current(10e-6)
        v1 = r1*i1
        self.assertIsInstance(v1, ac.Voltage)
        self.assertAlmostEqual(v1(1000), 0.22)

        r2 = r1*10
        self.assertIsInstance(r2, ac.Impedance)
        self.assertAlmostEqual(r2(1000), 220000)

        r3 = r2*(r1/(r1+r2))
        self.assertIsInstance(r3, ac.Impedance)
        self.assertAlmostEqual(r3(1000), 20000)

        r2 = 10*r1
        self.assertIsInstance(r2, ac.Impedance)
        self.assertAlmostEqual(r2(1000), 220000)

        v1 = i1*r1
        self.assertIsInstance(v1, ac.Voltage)
        self.assertAlmostEqual(v1(1000), 0.22)

        g1 = 1/r1
        self.assertIsInstance(g1, ac.Admittance)
        self.assertAlmostEqual(g1(1000), 4.54545e-05)

        r1 = ac.Impedance(r=22000)
        r2 = ac.Impedance(r=10000)
        x = r1/r2
        self.assertIsInstance(x, ac.Ratio)
        self.assertAlmostEqual(x(1000), 2.2)

        r3 = r1/10
        self.assertIsInstance(r3, ac.Impedance)
        self.assertAlmostEqual(r3(1000), 2200)

        g1 = 2/r1
        x = r1*g1
        self.assertIsInstance(x, ac.Ratio)
        self.assertAlmostEqual(x(1000), 2)

        Z = ac.Impedance(fw=lambda w: 3+4j)
        self.assertAlmostEqual(Z.abs(1000), 5)

        Z = ac.Impedance(fw=lambda w: 3-3j)
        self.assertAlmostEqual(Z.phase_deg(1000), -45)
        self.assertAlmostEqual(Z.phase_rad(1000), -math.pi/4)
        Z.properties(1000)

        Zr = ac.Impedance(r=1000)
        self.assertAlmostEqual(Zr(100), 1000)
        self.assertAlmostEqual(Zr(100000), 1000)

        Zl = ac.Impedance(l=0.01)
        self.assertAlmostEqual(Zl(100, 'rad/s'), 1j)
        self.assertAlmostEqual(Zl(100, 'krad/s'), 1000j)
        self.assertAlmostEqual(Zl(100, 'Hz'), 1j*2*math.pi)
        self.assertAlmostEqual(Zl(100, 'kHz'), 1000j*2*math.pi)

        Zc = ac.Impedance(c=1e-6)
        self.assertAlmostEqual(Zc(100, 'rad/s'), -1e4j)
        self.assertAlmostEqual(Zc(100, 'krad/s'), -10j)
        self.assertAlmostEqual(Zc(100, 'Hz'), -1e4j/(2*math.pi))
        self.assertAlmostEqual(Zc(100, 'kHz'), -10j/(2*math.pi))

        Z = Zr+Zl+Zc
        self.assertAlmostEqual(Z(100, 'rad/s'), 1000+1j-1e4j)
        self.assertAlmostEqual(Z(100, 'krad/s'), 1000+1000j-10j)
        z = (10*Z/10)*(5/Z)*Z*0.2 + 3*Z - Z/1 - 2/(1/Z)  # z=Z
        self.assertAlmostEqual(z(100, 'rad/s'), 1000+1j-1e4j)
        self.assertAlmostEqual(z(100, 'krad/s'), 1000+1000j-10j)

        z1 = ac.Impedance(fw=lambda w: 1000+1j*0.01*w-1j/(1e-6*w))
        self.assertAlmostEqual(z1(100, 'rad/s'), 1000+1j-1e4j)
        self.assertAlmostEqual(z1(100, 'krad/s'), 1000+1000j-10j)

    def test_class_Admittance(self):
        # test class Admittance

        G1 = ac.Admittance(r=1/0.1)
        self.assertIsInstance(G1, ac.Admittance)
        self.assertAlmostEqual(G1(1000), 0.1)
        G1
        print(G1)

        G2 = ac.Admittance(r=1/4.7e-3)
        self.assertIsInstance(G2, ac.Admittance)
        self.assertAlmostEqual(G2(1000), 4.7e-3)

        with self.assertRaises(ValueError):
            G = ac.Admittance(r=-1)

        G3 = G1+G2
        self.assertIsInstance(G3, ac.Admittance)
        self.assertAlmostEqual(G3(1000), 0.1047)

        G3 = G1//G2
        self.assertIsInstance(G3, ac.Admittance)
        self.assertAlmostEqual(G3(1000), 0.1047)

        G3 = G1-G2
        self.assertIsInstance(G3, ac.Admittance)
        self.assertAlmostEqual(G3(1000), 0.1-4.7e-3)

        G3 = +G1
        self.assertIsInstance(G3, ac.Admittance)
        self.assertIsNot(G3, G1)
        self.assertAlmostEqual(G3(1000), 0.1)

        G3 = -G1
        self.assertIsInstance(G3, ac.Admittance)
        self.assertAlmostEqual(G3(1000), -0.1)

        G3 = +G1-G2
        self.assertIsInstance(G3, ac.Admittance)
        self.assertAlmostEqual(G3(100), 0.1-4.7e-3)

        g1 = ac.Admittance(r=1/0.01)
        v1 = ac.Voltage(10)
        i1 = g1*v1
        self.assertIsInstance(i1, ac.Current)
        self.assertAlmostEqual(i1(100), 0.1)

        g2 = g1*10
        self.assertIsInstance(g2, ac.Admittance)
        self.assertAlmostEqual(g2(100), 0.1)

        g2 = 10*g1
        self.assertIsInstance(g2, ac.Admittance)
        self.assertAlmostEqual(g2(100), 0.1)

        g1 = ac.Admittance(r=1/0.001)
        r1 = 1/g1
        self.assertIsInstance(r1, ac.Impedance)
        self.assertAlmostEqual(r1(100), 1000)

        g1 = ac.Admittance(r=1/0.01)
        g2 = ac.Admittance(r=1/0.2)
        x = g1/g2
        self.assertIsInstance(x, ac.Ratio)
        self.assertAlmostEqual(x(100), 0.05)

        g3 = g1/10
        self.assertIsInstance(g3, ac.Admittance)
        self.assertAlmostEqual(g3(100), 0.001)

        g3 = g1//g2
        self.assertIsInstance(g3, ac.Admittance)
        self.assertAlmostEqual(g3(100), 0.21)

        r1 = 2/g1
        x = g1*r1
        self.assertIsInstance(x, ac.Ratio)
        self.assertAlmostEqual(x(100), 2)

        Y = ac.Admittance(fw=lambda w: 3+4j)
        self.assertAlmostEqual(Y.abs(1000), 5)

        Y = ac.Admittance(fw=lambda w: 3-3j)
        self.assertAlmostEqual(Y.phase_deg(1000), -45)
        self.assertAlmostEqual(Y.phase_rad(1000), -math.pi/4)
        Y.properties(1000)

        Yr = ac.Admittance(r=1000)
        self.assertAlmostEqual(Yr(100), 1e-3)
        self.assertAlmostEqual(Yr(100000), 1e-3)

        Yl = ac.Admittance(l=0.01)
        self.assertAlmostEqual(Yl(100, 'rad/s'), -1j)
        self.assertAlmostEqual(Yl(100, 'krad/s'), -1e-3j)
        self.assertAlmostEqual(Yl(100, 'Hz'), -1j/(2*math.pi))
        self.assertAlmostEqual(Yl(100, 'kHz'), -1j/(1000*2*math.pi))

        Yc = ac.Admittance(c=1e-6)
        self.assertAlmostEqual(Yc(100, 'rad/s'), 1e-4j)
        self.assertAlmostEqual(Yc(100, 'krad/s'), 0.1j)
        self.assertAlmostEqual(Yc(100, 'Hz'), 1/(-1e4j/(2*math.pi)))
        self.assertAlmostEqual(Yc(100, 'kHz'), 1/(-10j/(2*math.pi)))

        Y = Yr+Yl+Yc
        self.assertAlmostEqual(Y(100, 'rad/s'), 1e-3-1j+1e-4j)
        self.assertAlmostEqual(Y(100, 'krad/s'), 1e-3-1e-3j+0.1j)
        y = (10*Y/10)*(5/Y)*Y*0.2 + 3*Y - Y/1 - 2/(1/Y)  # y=Y
        self.assertAlmostEqual(y(100, 'rad/s'), 1e-3-1j+1e-4j)
        self.assertAlmostEqual(y(100, 'krad/s'), 1e-3-1e-3j+0.1j)

        z1 = ac.Admittance(fw=lambda w: 1e-3 - 100j/w + 1e-6j*w)
        self.assertAlmostEqual(z1(100, 'rad/s'), 1e-3-1j+1e-4j)
        self.assertAlmostEqual(z1(100, 'krad/s'), 1e-3-1e-3j+0.1j)

    def test_class_Law(self):
        # test class Law

        law = ac.Law()
        v1 = ac.Voltage(5)
        v2 = ac.Voltage(8)
        v3 = ac.Voltage(2.5)

        v4 = law.KVL('+--', v1, v2, v3)
        self.assertIsInstance(v4, ac.Voltage)
        self.assertAlmostEqual(v4(1000), -5.5)

        i1 = ac.Current(5e-3)
        i2 = ac.Current(8e-3)
        i3 = ac.Current(2.5e-3)

        i4 = law.KCL('-+-', i1, i2, i3)
        self.assertIsInstance(i4, ac.Current)
        self.assertAlmostEqual(i4(100), 0.0005)

        v1 = ac.Voltage(5)
        i1 = ac.Current(20e-6)
        r1 = law.Ohm(v=v1, i=i1)
        self.assertIsInstance(r1, ac.Impedance)
        self.assertAlmostEqual(r1(100), 250000)

        v2 = ac.Voltage(2)
        r2 = ac.Impedance(r=100e3)
        i2 = law.Ohm(v=v2, z=r2)
        self.assertIsInstance(i2, ac.Current)
        self.assertAlmostEqual(i2(100), 2e-5)

        i1 = ac.Current(20e-6)
        r2 = ac.Impedance(r=100e3)
        v2 = law.Ohm(z=r2, i=i1)
        self.assertIsInstance(v2, ac.Voltage)
        self.assertAlmostEqual(v2(100), 2)

        r1 = ac.Impedance(r=100e3)
        r2 = ac.Impedance(r=47e3)
        r3 = ac.Impedance(r=22e3)

        Req = law.Zserie(r1, r2, r3)
        self.assertIsInstance(Req, ac.Impedance)
        self.assertAlmostEqual(Req(1000), 169000)

        r1 = ac.Impedance(r=1500)
        r2 = ac.Impedance(r=1000)

        Req = law.Zparallel(r1, r2)
        self.assertIsInstance(Req, ac.Impedance)
        self.assertAlmostEqual(Req(1000), 600)

        r1 = ac.Impedance(r=1000)
        r2 = ac.Impedance(r=9000)
        v = ac.Voltage(5)

        v1 = law.VoltageDivider(vtotal=v, z=r1, z2=r2)
        self.assertIsInstance(v1, ac.Voltage)
        self.assertAlmostEqual(v1(1000), 0.5)

        r1 = ac.Impedance(r=100)
        r2 = ac.Impedance(r=900)
        i = ac.Current(100e-3)

        i1 = law.CurrentDivider(itotal=i, z=r1, z2=r2)
        self.assertIsInstance(i1, ac.Current)
        self.assertAlmostEqual(i1(1000), 0.09)

        masse = ac.Voltage(0)
        E = ac.Voltage(10)
        R1 = ac.Impedance(r=1000)
        R2 = ac.Impedance(r=10000)
        R3 = ac.Impedance(r=2200)

        v2 = law.Millman(v_z=[(E, R1), (masse, R2), (masse, R3)])
        self.assertIsInstance(v2, ac.Voltage)
        self.assertAlmostEqual(v2(1000), 6.4327485)

        v1 = ac.Voltage(5)
        i1 = ac.Current(100e-3)
        p1 = law.Power(v=v1, i=i1)
        self.assertIsInstance(p1, ac.Power)
        self.assertAlmostEqual(p1(1000), 0.5)

        r1 = ac.Impedance(r=50)
        i1 = ac.Current(100e-3)
        p1 = law.Joule(z=r1, i=i1)
        self.assertIsInstance(p1, ac.Power)
        self.assertAlmostEqual(p1(1000), 0.5)

        r2 = ac.Impedance(r=220)
        v2 = ac.Voltage(5)
        p2 = law.Joule(z=r2, v=v2)
        self.assertIsInstance(p2, ac.Power)
        self.assertAlmostEqual(p2(1000), 0.1136363636)

    def test_class_Voltage(self):
        # test class Voltage
        v1 = ac.Voltage(0.032)
        v1
        print(v1)
        self.assertIsInstance(v1, ac.Voltage)
        self.assertAlmostEqual(v1(1000), 0.032)

        v2 = ac.Voltage(100e-3)
        self.assertIsInstance(v2, ac.Voltage)
        self.assertAlmostEqual(v2(1000), 0.1)

        v1 = ac.Voltage(5)
        v2 = ac.Voltage(3)
        v3 = v1+v2
        self.assertIsInstance(v3, ac.Voltage)
        self.assertAlmostEqual(v3(1000), 8)

        v3 = v1-v2
        self.assertIsInstance(v3, ac.Voltage)
        self.assertAlmostEqual(v3(100), 2)

        v3 = +v1+v2
        self.assertIsInstance(v3, ac.Voltage)
        self.assertAlmostEqual(v3(10), 8)

        v3 = -v2+v1
        self.assertIsInstance(v3, ac.Voltage)
        self.assertAlmostEqual(v3(10), 2)

        v1 = ac.Voltage(5)
        r1 = ac.Impedance(r=1000)
        i1 = v1/r1
        self.assertIsInstance(i1, ac.Current)
        self.assertAlmostEqual(i1(10), 5e-3)

        v1 = ac.Voltage(5)
        i1 = ac.Current(5e-3)
        r1 = v1/i1
        self.assertIsInstance(r1, ac.Impedance)
        self.assertAlmostEqual(r1(100), 1000)

        v2 = v1/10
        self.assertIsInstance(v2, ac.Voltage)
        self.assertAlmostEqual(v2(100), 0.5)

        x = v1/v2
        self.assertIsInstance(x, ac.Ratio)
        self.assertAlmostEqual(x(100), 10)

        g1 = ac.Admittance(r=1/0.001)
        i1 = v1*g1
        self.assertIsInstance(i1, ac.Current)
        self.assertAlmostEqual(i1(100), 5e-3)

        p1 = v1*i1
        self.assertIsInstance(p1, ac.Power)
        self.assertAlmostEqual(p1(100), 25e-3)

        v2 = v1*0.1
        self.assertIsInstance(v2, ac.Voltage)
        self.assertAlmostEqual(v2(10), 0.5)

        v2 = 0.1*v1
        self.assertIsInstance(v2, ac.Voltage)
        self.assertAlmostEqual(v2(100), 0.5)

        gnd = ac.Voltage(0)
        with self.assertRaises(ZeroDivisionError):
            x = v1/gnd
            x(100)

        i0 = ac.Current(0)
        with self.assertRaises(ZeroDivisionError):
            r = v1/i0
            r(100)

        with self.assertRaises(ZeroDivisionError):
            v = v1/ac.Ratio(0)
            v(100)

        v = ac.Voltage(fw=lambda w: 8+6j)
        self.assertAlmostEqual(v.rms(1000), 10)
        self.assertAlmostEqual(v.amplitude(1000), 10*math.sqrt(2))
        self.assertAlmostEqual(v.dbv(1000), 20)

        v = ac.Voltage(fw=lambda w: 3-3j)
        self.assertAlmostEqual(v.phase_deg(1000), -45)
        self.assertAlmostEqual(v.phase_rad(1000), -math.pi/4)
        v.properties(1000)

        v = ac.Voltage(3*math.sqrt(2), 45)  # 3+3j
        self.assertAlmostEqual(v.rms(1000), 3*math.sqrt(2))
        self.assertAlmostEqual(v.amplitude(1000), 6)
        self.assertAlmostEqual(v.phase_deg(1000), 45)
        self.assertAlmostEqual(v.phase_rad(1000), math.pi/4)

    def test_class_Current(self):
        # test class Current

        i1 = ac.Current(32e-3, 0)
        self.assertIsInstance(i1, ac.Current)
        self.assertAlmostEqual(i1(100), 32e-3)
        i1
        print(i1)

        i2 = ac.Current(0.050)
        i3 = i1+i2
        self.assertIsInstance(i3, ac.Current)
        self.assertAlmostEqual(i3(100), 82e-3)

        i3 = i1-i2
        self.assertIsInstance(i3, ac.Current)
        self.assertAlmostEqual(i3(100), -18e-3)

        i3 = +i1+i2
        self.assertIsInstance(i3, ac.Current)
        self.assertAlmostEqual(i3(100), 82e-3)

        i3 = -i2+i1
        self.assertIsInstance(i3, ac.Current)
        self.assertAlmostEqual(i3(100), -18e-3)

        i2 = i1/10
        self.assertIsInstance(i2, ac.Current)
        self.assertAlmostEqual(i2(100), 3.2e-3)

        x = i1/i2
        self.assertIsInstance(x, ac.Ratio)
        self.assertAlmostEqual(x(100), 10)

        i2 = i1*0.1
        self.assertIsInstance(i2, ac.Current)
        self.assertAlmostEqual(i2(100), 3.2e-3)

        i2 = 0.1*i1
        self.assertIsInstance(i2, ac.Current)
        self.assertAlmostEqual(i2(100), 3.2e-3)

        v1 = ac.Voltage(10)
        i1 = ac.Current(0.01)
        g1 = i1/v1
        self.assertIsInstance(g1, ac.Admittance)
        self.assertAlmostEqual(g1(99), 0.001)

        gnd = ac.Voltage(0)
        with self.assertRaises(ZeroDivisionError):
            g = i1/gnd
            g(100)

        i0 = ac.Current(0)
        with self.assertRaises(ZeroDivisionError):
            x = i1/i0
            x(100)

        with self.assertRaises(ZeroDivisionError):
            i = i1/0
            i(100)

        i = ac.Current(fw=lambda w: 8+6j)
        self.assertAlmostEqual(i.rms(1000), 10)
        self.assertAlmostEqual(i.amplitude(1000), 10*math.sqrt(2))
        self.assertAlmostEqual(i.dba(1000), 20)

        i = ac.Current(fw=lambda w: 3-3j)
        self.assertAlmostEqual(i.phase_deg(1000), -45)
        self.assertAlmostEqual(i.phase_rad(1000), -math.pi/4)
        i.properties(1000)

        i = ac.Current(3*math.sqrt(2), 45)  # 3+3j
        self.assertAlmostEqual(i.rms(1000), 3*math.sqrt(2))
        self.assertAlmostEqual(i.amplitude(1000), 6)
        self.assertAlmostEqual(i.phase_deg(1000), 45)
        self.assertAlmostEqual(i.phase_rad(1000), math.pi/4)

    def test_class_Power(self):
        # test class Power
        p1 = ac.Power(10e6)
        p1
        print(p1)
        self.assertIsInstance(p1, ac.Power)
        self.assertAlmostEqual(p1(99), 10e6)

        p2 = ac.Power(2.5e6)
        p3 = p1+p2
        self.assertIsInstance(p3, ac.Power)
        self.assertAlmostEqual(p3(99), 12.5e6)

        p3 = +p1+p2
        self.assertIsInstance(p3, ac.Power)
        self.assertAlmostEqual(p3(99), 12.5e6)

        p3 = p1-p2
        self.assertIsInstance(p3, ac.Power)
        self.assertAlmostEqual(p3(99), 7.5e6)

        p3 = -p2+p1
        self.assertIsInstance(p3, ac.Power)
        self.assertAlmostEqual(p3(99), 7.5e6)

        p1 = ac.Power(0.05)
        v1 = ac.Voltage(10)
        i1 = p1/v1
        self.assertIsInstance(i1, ac.Current)
        self.assertAlmostEqual(i1(99), 5e-3)

        p2 = p1/5
        self.assertIsInstance(p2, ac.Power)
        self.assertAlmostEqual(p2(99), 0.01)

        v2 = p2/i1
        self.assertIsInstance(v2, ac.Voltage)
        self.assertAlmostEqual(v2(99), 2)

        x = p1/p2
        self.assertIsInstance(x, ac.Ratio)
        self.assertAlmostEqual(x(100), 5)

        p2 = p1*10
        self.assertIsInstance(p2, ac.Power)
        self.assertAlmostEqual(p2(99), 0.5)

        p2 = 10.0*p1
        self.assertIsInstance(p2, ac.Power)
        self.assertAlmostEqual(p2(99), 0.5)

        p = ac.Power(fw=lambda w: 8+6j)
        self.assertAlmostEqual(p.va(1000), 10)
        self.assertAlmostEqual(p.var(1000), 6)
        self.assertAlmostEqual(p.w(1000), 8)
        self.assertAlmostEqual(p.pf(1000), 0.8)

        p = ac.Power(fw=lambda w: 100-100j)
        self.assertAlmostEqual(p.phase_deg(1000), -45)
        self.assertAlmostEqual(p.phase_rad(1000), -math.pi/4)
        self.assertAlmostEqual(p.dbw(1000), 20)
        p.properties(1000)

        p = ac.Power(100+100j)
        self.assertAlmostEqual(p.phase_deg(1000), 45)
        self.assertAlmostEqual(p.phase_rad(1000), math.pi/4)
        self.assertAlmostEqual(p.dbw(1000), 20)

        v = ac.Voltage(5, 30)
        i = ac.Current(2, -20)

        s0 = v*i
        s = i*v
        self.assertAlmostEqual(s(FREQ), s0(FREQ))
        # s.properties(1000, message="s.properties")
        i1 = s/v
        self.assertAlmostEqual(i1(FREQ), i(FREQ))
        # i1.properties(1000, message="i1.properties")
        u = s/i
        self.assertAlmostEqual(u(FREQ), v(FREQ))
        # u.properties(1000, message="u.properties")
        s1 = u*i1
        self.assertAlmostEqual(s1(FREQ), s0(FREQ))
        # s1.properties(1000, message="s1.properties")
        z = v/i
        # z.properties(1000, message="z.properties")
        s2 = z*i*i
        self.assertAlmostEqual(s2(FREQ), s0(FREQ))
        # s2.properties(1000, message="s2.properties")
        s3 = u*(u/z)
        self.assertAlmostEqual(s3(FREQ), s0(FREQ))
        # s3.properties(1000, message="s3.properties")

        law = ac.Law()
        s4 = law.Joule(z=z, v=v)
        self.assertIsInstance(s4, ac.Power)
        self.assertAlmostEqual(s4(FREQ), s0(FREQ))

        s5 = law.Joule(z=z, i=i)
        self.assertIsInstance(s5, ac.Power)
        self.assertAlmostEqual(s5(FREQ), s0(FREQ))

    def test_class_Ratio(self):
        x = ac.Ratio(100+100j)
        self.assertAlmostEqual(x(99), 100+100j)
        self.assertAlmostEqual(x.phase_deg(1000), 45)
        self.assertAlmostEqual(x.phase_rad(1000), math.pi/4)
        x
        print(x)

        x = ac.Ratio(fw=lambda w: 8+6j)
        self.assertAlmostEqual(x.abs(1000), 10)
        self.assertAlmostEqual(x.db(1000), 20)
        self.assertAlmostEqual(x.dbw(1000), 10)
        x.properties(1000)

    def test_mul(self):
        # multiplication
        matrice = dict()  # unit matrix

        # label
        f = "complex"
        y = "Admittance"
        i = "Current"
        p = "Power"
        z = "Impedance"
        r = "Ratio"
        v = "Voltage"

        nc = "nc"  # not to do
        typeerr = "TypeError"
        diverr = 'ZeroDivisionError'

        # f, y, i, p, z, r, v
        matrice[f, f], matrice[f, y], matrice[f, i], matrice[f, p], matrice[f, z], matrice[f, r], matrice[f, v] = f, y, i, p, z, r, v

        # f, y, i, p, z, r, v
        matrice[y, f], matrice[y, y], matrice[y, i], matrice[y, p], matrice[y, z], matrice[y, r], matrice[y, v] = y, typeerr, typeerr, typeerr, r, y, i

        # f, y, i, p, z, r, v
        matrice[i, f], matrice[i, y], matrice[i, i], matrice[i, p], matrice[i, z], matrice[i, r], matrice[i, v] = i, typeerr, typeerr, typeerr, v, i, nc

        # f, y, i, p, z, r, v
        matrice[p, f], matrice[p, y], matrice[p, i], matrice[p, p], matrice[p, z], matrice[p, r], matrice[p, v] = p, typeerr, typeerr, typeerr, typeerr, p, typeerr

        # f, y, i, p, z, r, v
        matrice[z, f], matrice[z, y], matrice[z, i], matrice[z, p], matrice[z, z], matrice[z, r], matrice[z, v] = z, r, v, typeerr, typeerr, z, typeerr

        # f, y, i, p, z, r, v
        matrice[r, f], matrice[r, y], matrice[r, i], matrice[r, p], matrice[r, z], matrice[r, r], matrice[r, v] = r, y, i, p, z, r, v

        # f, y, i, p, z, r, v
        matrice[v, f], matrice[v, y], matrice[v, i], matrice[v, p], matrice[v, z], matrice[v, r], matrice[v, v] = v, i, nc, typeerr, typeerr, v, typeerr

        for i in matrice:
            arg1, arg2 = i
            # print(arg1, arg2)

            obj1 = dict1_obj[arg1]
            obj2 = dict2_obj[arg2]

            print(arg1, '*', arg2, '->', matrice[i])

            if matrice[i] == typeerr:
                with self.assertRaises(TypeError):
                    obj1*obj2
            elif matrice[i] == diverr:
                with self.assertRaises(ZeroDivisionError):
                    obj1*obj2
            elif matrice[i] == nc:
                pass
            else:
                obj = obj1*obj2
                self.assertIsInstance(obj, dict1_obj[matrice[i]].__class__)
                if isinstance(obj1, (int, float, complex)):
                    # no callable
                    val1 = obj1
                else:
                    val1 = obj1(FREQ)
                if isinstance(obj2, (int, float, complex)):
                    val2 = obj2
                else:
                    val2 = obj2(FREQ)

                val = val1*val2  # expected value

                if isinstance(obj, (int, float, complex)):
                    self.assertAlmostEqual(obj, val)
                else:
                    self.assertAlmostEqual(obj(FREQ), val)

    def test_rmul(self):
        # right multiplication
        matrice = dict()  # unit matrix

        # label
        f = "complex"
        y = "Admittance"
        i = "Current"
        p = "Power"
        z = "Impedance"
        r = "Ratio"
        v = "Voltage"

        nc = "nc"  # not to do
        typeerr = "TypeError"
        diverr = 'ZeroDivisionError'

        # f, y, i, p, z, r, v
        matrice[f, y], matrice[f, i], matrice[f, p], matrice[f, z], matrice[f, r], matrice[f, v] = y, i, p, z, r, v

        for i in matrice:
            arg1, arg2 = i
            # print(arg1, arg2)

            obj1 = dict1_obj[arg1]
            obj2 = dict2_obj[arg2]

            print(arg1, '* ', arg2, '->', matrice[i])

            if matrice[i] == typeerr:
                with self.assertRaises(TypeError):
                    obj1*obj2
            elif matrice[i] == diverr:
                with self.assertRaises(ZeroDivisionError):
                    obj1*obj2
            elif matrice[i] == nc:
                pass
            else:
                obj = obj1*obj2
                self.assertIsInstance(obj, dict1_obj[matrice[i]].__class__)
                if isinstance(obj1, (int, float, complex)):
                    # no callable
                    val1 = obj1
                else:
                    val1 = obj1(FREQ)
                if isinstance(obj2, (int, float, complex)):
                    val2 = obj2
                else:
                    val2 = obj2(FREQ)
                val = val1*val2  # expected value

                if isinstance(obj, (int, float, complex)):
                    self.assertAlmostEqual(obj, val)
                else:
                    self.assertAlmostEqual(obj(FREQ), val)

    def test_add(self):
        # addition
        matrice = dict()  # unit matrix

        # label
        f = "complex"  # and complex
        y = "Admittance"
        i = "Current"
        p = "Power"
        z = "Impedance"
        r = "Ratio"
        v = "Voltage"

        nc = "nc"  # not to do
        typeerr = "TypeError"
        diverr = 'ZeroDivisionError'

        # f, y, i, p, z, r, v
        matrice[f, f], matrice[f, y], matrice[f, i], matrice[f, p], matrice[f, z], matrice[f, r], matrice[f, v] = f, typeerr, typeerr, typeerr, typeerr, r, typeerr

        # f, y, i, p, z, r, v
        matrice[y, f], matrice[y, y], matrice[y, i], matrice[y, p], matrice[y, z], matrice[y, r], matrice[y, v] = typeerr, y, typeerr, typeerr, typeerr, typeerr, typeerr

        # f, y, i, p, z, r, v
        matrice[i, f], matrice[i, y], matrice[i, i], matrice[i, p], matrice[i, z], matrice[i, r], matrice[i, v] = typeerr, typeerr, i, typeerr, typeerr, typeerr, typeerr

        # f, y, i, p, z, r, v
        matrice[p, f], matrice[p, y], matrice[p, i], matrice[p, p], matrice[p, z], matrice[p, r], matrice[p, v] = typeerr, typeerr, typeerr, p, typeerr, typeerr, typeerr

        # f, y, i, p, z, r, v
        matrice[z, f], matrice[z, y], matrice[z, i], matrice[z, p], matrice[z, z], matrice[z, r], matrice[z, v] = typeerr, typeerr, typeerr, typeerr, z, typeerr, typeerr

        # f, y, i, p, z, r, v
        matrice[r, f], matrice[r, y], matrice[r, i], matrice[r, p], matrice[r, z], matrice[r, r], matrice[r, v] = r, typeerr, typeerr, typeerr, typeerr, r, typeerr

        # f, y, i, p, z, r, v
        matrice[v, f], matrice[v, y], matrice[v, i], matrice[v, p], matrice[v, z], matrice[v, r], matrice[v, v] = typeerr, typeerr, typeerr, typeerr, typeerr, typeerr, v

        for i in matrice:
            arg1, arg2 = i
            # print(arg1, arg2)

            obj1 = dict1_obj[arg1]
            obj2 = dict2_obj[arg2]

            print(arg1, '+', arg2, '->', matrice[i])

            if matrice[i] == typeerr:
                with self.assertRaises(TypeError):
                    obj1+obj2
            elif matrice[i] == diverr:
                with self.assertRaises(ZeroDivisionError):
                    obj1+obj2
            elif matrice[i] == nc:
                pass
            else:
                obj = obj1+obj2
                self.assertIsInstance(obj, dict1_obj[matrice[i]].__class__)
                if isinstance(obj1, (int, float, complex)):
                    # no callable
                    val1 = obj1
                else:
                    val1 = obj1(FREQ)
                if isinstance(obj2, (int, float, complex)):
                    val2 = obj2
                else:
                    val2 = obj2(FREQ)
                val = val1+val2  # expected value

                if isinstance(obj, (int, float, complex)):
                    self.assertAlmostEqual(obj, val)
                else:
                    self.assertAlmostEqual(obj(FREQ), val)

    def test_sub(self):
        # substraction
        matrice = dict()  # unit matrix

        # label
        f = "complex"  # and complex
        y = "Admittance"
        i = "Current"
        p = "Power"
        z = "Impedance"
        r = "Ratio"
        v = "Voltage"

        nc = "nc"  # not to do
        typeerr = "TypeError"
        diverr = 'ZeroDivisionError'

        # f, y, i, p, z, r, v
        matrice[f, f], matrice[f, y], matrice[f, i], matrice[f, p], matrice[f, z], matrice[f, r], matrice[f, v] = f, typeerr, typeerr, typeerr, typeerr, r, typeerr

        # f, y, i, p, z, r, v
        matrice[y, f], matrice[y, y], matrice[y, i], matrice[y, p], matrice[y, z], matrice[y, r], matrice[y, v] = typeerr, y, typeerr, typeerr, typeerr, typeerr, typeerr

        # f, y, i, p, z, r, v
        matrice[i, f], matrice[i, y], matrice[i, i], matrice[i, p], matrice[i, z], matrice[i, r], matrice[i, v] = typeerr, typeerr, i, typeerr, typeerr, typeerr, typeerr

        # f, y, i, p, z, r, v
        matrice[p, f], matrice[p, y], matrice[p, i], matrice[p, p], matrice[p, z], matrice[p, r], matrice[p, v] = typeerr, typeerr, typeerr, p, typeerr, typeerr, typeerr

        # f, y, i, p, z, r, v
        matrice[z, f], matrice[z, y], matrice[z, i], matrice[z, p], matrice[z, z], matrice[z, r], matrice[z, v] = typeerr, typeerr, typeerr, typeerr, z, typeerr, typeerr

        # f, y, i, p, z, r, v
        matrice[r, f], matrice[r, y], matrice[r, i], matrice[r, p], matrice[r, z], matrice[r, r], matrice[r, v] = r, typeerr, typeerr, typeerr, typeerr, r, typeerr

        # f, y, i, p, z, r, v
        matrice[v, f], matrice[v, y], matrice[v, i], matrice[v, p], matrice[v, z], matrice[v, r], matrice[v, v] = typeerr, typeerr, typeerr, typeerr, typeerr, typeerr, v

        for i in matrice:
            arg1, arg2 = i
            # print(arg1, arg2)

            obj1 = dict1_obj[arg1]
            obj2 = dict2_obj[arg2]

            print(arg1, '-', arg2, '->', matrice[i])

            if matrice[i] == typeerr:
                with self.assertRaises(TypeError):
                    obj1-obj2
            elif matrice[i] == diverr:
                with self.assertRaises(ZeroDivisionError):
                    obj1-obj2
            elif matrice[i] == nc:
                pass
            else:
                obj = obj1-obj2
                self.assertIsInstance(obj, dict1_obj[matrice[i]].__class__)
                if isinstance(obj1, (int, float, complex)):
                    # no callable
                    val1 = obj1
                else:
                    val1 = obj1(FREQ)
                if isinstance(obj2, (int, float, complex)):
                    val2 = obj2
                else:
                    val2 = obj2(FREQ)
                val = val1-val2  # expected value

                if isinstance(obj, (int, float, complex)):
                    self.assertAlmostEqual(obj, val)
                else:
                    self.assertAlmostEqual(obj(FREQ), val)

    def test_pos(self):
        # +self
        matrice = dict()  # unit matrix

        # label
        f = "complex"  # and complex
        y = "Admittance"
        i = "Current"
        p = "Power"
        z = "Impedance"
        r = "Ratio"
        v = "Voltage"

        nc = "nc"  # not to do
        typeerr = "TypeError"
        diverr = 'ZeroDivisionError'

        # f, y, i, p, z, r, v
        matrice[f], matrice[y], matrice[i], matrice[p], matrice[z], matrice[r], matrice[v] = f, y, i, p, z, r, v

        for i in matrice:
            arg1 = i
            # print(arg1, arg2)

            obj1 = dict1_obj[arg1]

            print('+', arg1, '->', matrice[i])

            if matrice[i] == typeerr:
                with self.assertRaises(TypeError):
                    +obj1
            elif matrice[i] == diverr:
                with self.assertRaises(ZeroDivisionError):
                    +obj1
            elif matrice[i] == nc:
                pass
            else:
                obj = +obj1
                self.assertIsInstance(obj, dict1_obj[matrice[i]].__class__)
                if isinstance(obj1, (int, float, complex)):
                    # no callable
                    val1 = obj1
                else:
                    val1 = obj1(FREQ)

                val = val1  # expected value

                if isinstance(obj, (int, float, complex)):
                    self.assertAlmostEqual(obj, val)
                else:
                    self.assertAlmostEqual(obj(FREQ), val)

    def test_neg(self):
        # -self
        matrice = dict()  # unit matrix

        # label
        f = "complex"  # and complex
        y = "Admittance"
        i = "Current"
        p = "Power"
        z = "Impedance"
        r = "Ratio"
        v = "Voltage"

        nc = "nc"  # not to do
        typeerr = "TypeError"
        diverr = 'ZeroDivisionError'

        # f, y, i, p, z, r, v
        matrice[f], matrice[y], matrice[i], matrice[p], matrice[z], matrice[r], matrice[v] = f, y, i, p, z, r, v

        for i in matrice:
            arg1 = i
            # print(arg1, arg2)

            obj1 = dict1_obj[arg1]

            print('-', arg1, '->', matrice[i])

            if matrice[i] == typeerr:
                with self.assertRaises(TypeError):
                    -obj1
            elif matrice[i] == diverr:
                with self.assertRaises(ZeroDivisionError):
                    -obj1
            elif matrice[i] == nc:
                pass
            else:
                obj = -obj1
                self.assertIsInstance(obj, dict1_obj[matrice[i]].__class__)
                if isinstance(obj1, (int, float, complex)):
                    # no callable
                    val1 = obj1
                else:
                    val1 = obj1(FREQ)

                val = -val1  # expected value

                if isinstance(obj, (int, float, complex)):
                    self.assertAlmostEqual(obj, val)
                else:
                    self.assertAlmostEqual(obj(FREQ), val)

    def test_rtruediv(self):
        # right division
        matrice = dict()  # unit matrix

        # label
        f = "complex"  # and complex
        y = "Admittance"
        i = "Current"
        p = "Power"
        z = "Impedance"
        r = "Ratio"
        v = "Voltage"

        nc = "nc"  # not to do
        typeerr = "TypeError"
        diverr = 'ZeroDivisionError'

        # f, y, i, p, z, r, v
        matrice[f, y], matrice[f, i], matrice[f, p], matrice[f, z], matrice[f, r], matrice[f, v] = z, typeerr, typeerr, y, r, typeerr

        for i in matrice:
            arg1, arg2 = i
            # print(arg1, arg2)

            obj1 = dict1_obj[arg1]
            obj2 = dict2_obj[arg2]

            print(arg1, '/ ', arg2, '->', matrice[i])

            if matrice[i] == typeerr:
                with self.assertRaises(TypeError):
                    obj1/obj2
            elif matrice[i] == diverr:
                with self.assertRaises(ZeroDivisionError):
                    obj1/obj2
            elif matrice[i] == nc:
                pass
            else:
                obj = obj1/obj2
                self.assertIsInstance(obj, dict1_obj[matrice[i]].__class__)
                if isinstance(obj1, (int, float, complex)):
                    # no callable
                    val1 = obj1
                else:
                    val1 = obj1(FREQ)
                if isinstance(obj2, (int, float, complex)):
                    val2 = obj2
                else:
                    val2 = obj2(FREQ)
                val = val1/val2  # expected value

                if isinstance(obj, (int, float, complex)):
                    self.assertAlmostEqual(obj, val)
                else:
                    self.assertAlmostEqual(obj(FREQ), val)

    def test_truediv(self):
        # division
        matrice = dict()  # unit matrix

        # label
        f = "complex"  # and complex
        y = "Admittance"
        i = "Current"
        p = "Power"
        z = "Impedance"
        r = "Ratio"
        v = "Voltage"

        nc = "nc"  # not to do
        typeerr = "TypeError"
        diverr = 'ZeroDivisionError'

        # f, y, i, p, z, r, v
        matrice[f, f], matrice[f, y], matrice[f, i], matrice[f, p], matrice[f, z], matrice[f, r], matrice[f, v] = f, z, typeerr, typeerr, y, r, typeerr

        # f, y, i, p, z, r, v
        matrice[y, f], matrice[y, y], matrice[y, i], matrice[y, p], matrice[y, z], matrice[y, r], matrice[y, v] = y, r, typeerr, typeerr, typeerr, y, typeerr

        # f, y, i, p, z, r, v
        matrice[i, f], matrice[i, y], matrice[i, i], matrice[i, p], matrice[i, z], matrice[i, r], matrice[i, v] = i, v, r, typeerr, typeerr, i, y

        # f, y, i, p, z, r, v
        matrice[p, f], matrice[p, y], matrice[p, i], matrice[p, p], matrice[p, z], matrice[p, r], matrice[p, v] = p, typeerr, nc, r, typeerr, p, nc

        # f, y, i, p, z, r, v
        matrice[z, f], matrice[z, y], matrice[z, i], matrice[z, p], matrice[z, z], matrice[z, r], matrice[z, v] = z, typeerr, typeerr, typeerr, r, z, typeerr

        # f, y, i, p, z, r, v
        matrice[r, f], matrice[r, y], matrice[r, i], matrice[r, p], matrice[r, z], matrice[r, r], matrice[r, v] = r, z, typeerr, typeerr, y, r, typeerr

        # f, y, i, p, z, r, v
        matrice[v, f], matrice[v, y], matrice[v, i], matrice[v, p], matrice[v, z], matrice[v, r], matrice[v, v] = v, typeerr, z, typeerr, i, v, r

        for i in matrice:
            arg1, arg2 = i
            # print(arg1, arg2)

            obj1 = dict1_obj[arg1]
            obj2 = dict2_obj[arg2]

            print(arg1, '/', arg2, '->', matrice[i])

            if matrice[i] == typeerr:
                with self.assertRaises(TypeError):
                    obj1/obj2
            elif matrice[i] == diverr:
                with self.assertRaises(ZeroDivisionError):
                    obj1/obj2
            elif matrice[i] == nc:
                pass
            else:
                obj = obj1/obj2
                self.assertIsInstance(obj, dict1_obj[matrice[i]].__class__)
                if isinstance(obj1, (int, float, complex)):
                    # no callable
                    val1 = obj1
                else:
                    val1 = obj1(FREQ)
                if isinstance(obj2, (int, float, complex)):
                    val2 = obj2
                else:
                    val2 = obj2(FREQ)
                val = val1/val2  # expected value

                if isinstance(obj, (int, float, complex)):
                    self.assertAlmostEqual(obj, val)
                else:
                    self.assertAlmostEqual(obj(FREQ), val)

    def test_filter_rc_bandpass_1er_order(self):
        Vin = ac.Voltage(5, 45)
        Zr = ac.Impedance(r=1e3)
        Zc = ac.Impedance(c=100e-9)
        Zt = Zr+Zc
        I = Vin/Zt
        Vout = Zc*I
        H1 = Vout/Vin
        # H1.bode(title='H1')
        H2 = Zc/(Zr+Zc)
        # H2.bode(title='H2')
        H3 = ac.Ratio(fw=lambda w: 1/(1+1j*w/10000))
        # H3.bode(title='H3')

        # Zr.bode(title='Zr')
        # Zc.bode(title='Zc')
        # I.bode(title='I',yscale='linear', magnitude_unit='default')
        # Vout.bode(title='Vout',yscale='linear', magnitude_unit='default')

        # test rlc serie
        Zl = ac.Impedance(l=0.1)
        Zt1 = Zr+Zc+Zl
        # Zt1.bode(title='Zt1', yscale='log')
        I = Vin/Zt1
        # I.bode(title='I',yscale='linear', magnitude_unit='default')

        ac.show()

    def test_attributs(self):
        # ref
        Vin = ac.Voltage(5)  # Vrms
        Zr1 = ac.Impedance(r=180)
        Zl1 = ac.Impedance(l=0.1)
        Zr2 = ac.Impedance(r=2200)
        Zc1 = ac.Impedance(c=330e-9)

        Zeq1 = 1/(1/Zr2 + 1/Zc1)
        Zeq = Zr1 + Zl1 + Zeq1  # impedances in series
        IL = Vin/Zeq
        Vout = IL*Zeq1  # Ohm's law
        VL = Vin-Vout  # Kirchhoff’s voltage law
        IC = Vout/Zc1  # Ohm's law
        IR = IL-IC  # Kirchhoff’s current law
        H = Vout/Vin

        Vin_ = ac.Voltage(8, 30)  # Vrms
        Zr1_ = ac.Impedance(r=2)
        Yl1_ = ac.Admittance(l=0.11)
        Zr2_ = ac.Impedance(r=200)
        Zc1_ = ac.Impedance(c=330e-6)

        Zl1_ = 1/Yl1_
        Zeq1_ = Zr2_//Zc1_
        Zeq_ = Zr1_ + Zl1_ + Zeq1_  # impedances in series
        IL_ = Vin_/Zeq_
        Vout_ = IL_*Zeq1_  # Ohm's law
        VL_ = Vin_-Vout_  # Kirchhoff’s voltage law
        IR_ = Vout_/Zr2_  # Ohm's law
        IC_ = IL_-IR_  # Kirchhoff’s current law
        H_ = Vout_/Vin_

        Vin_.RMS = 5
        Vin_.phase = 0
        Zr1_.r = 180
        Yl1_.l = 0.1
        Zr2_.r = 2200
        Zc1_.c = 330e-9

        self.assertAlmostEqual(Vin(100), Vin_(100))
        self.assertAlmostEqual(Zr1(100), Zr1_(100))
        self.assertAlmostEqual(Zl1(100), Zl1_(100))
        self.assertAlmostEqual(Zr2(100), Zr2_(100))
        self.assertAlmostEqual(Zc1(100), Zc1_(100))

        self.assertAlmostEqual(Zeq1(100), Zeq1_(100))
        self.assertAlmostEqual(Zeq(100), Zeq_(100))
        self.assertAlmostEqual(IL(100), IL_(100))
        self.assertAlmostEqual(Vout(100), Vout_(100))
        self.assertAlmostEqual(VL(100), VL_(100))
        self.assertAlmostEqual(IR(100), IR_(100))
        self.assertAlmostEqual(IC(100), IC_(100))
        self.assertAlmostEqual(H(100), H_(100))

if __name__ == '__main__':
    unittest.main()
